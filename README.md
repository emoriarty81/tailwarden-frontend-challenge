## Tailwarden Frontend Test

### Start

Install Horizon UI by running either of the following:

```bash
git clone https://gitlab.com/emoriarty81/tailwarden-frontend-challenge.git
```

Run in terminal this command:

```bash
npm install
```

Then run this command to start your local server

```bash
npm start
```

You should see the following dashboard at http://localhost:3000
![dashboard preview](./doc/preview.png)
