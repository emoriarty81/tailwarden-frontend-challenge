// Chakra imports
import { Portal, Box, useDisclosure } from "@chakra-ui/react";
import Footer from "components/footer/FooterAdmin.js";
// Layout components
import Navbar from "components/navbar/NavbarAdmin.js";
import React, { useState, useEffect } from "react";
import UserReports from "views/admin/default";
import {
  useDataDispatch,
  SET_ACCOUNTS,
  CHANGE_CURRENT_ACCOUNT,
  SET_CURRENT_STATS,
  SET_CURRENT_HISTORY,
  SET_STATS,
  useData,
} from "contexts/DataContext";

// Custom Chakra theme
export default function Dashboard(props) {
  const { ...rest } = props;
  // states and functions
  const [fixed] = useState(true);
  const dispatchData = useDataDispatch();
  const { currentAccount, accounts } = useData();

  useEffect(() => {
    // fetch data
    const dataFetch = async () => {
      const accounts = await (
        await fetch("https://hiring.tailwarden.com/v1/accounts")
      ).json();

      // set state when the data received
      dispatchData({ type: SET_ACCOUNTS, accounts });
      dispatchData({ type: CHANGE_CURRENT_ACCOUNT, account: accounts[0] });
    };

    dataFetch();
  }, [dispatchData]);

  useEffect(() => {
    // fetch data
    const dataFetch = async () => {
      const otherAccounts = accounts.filter(
        (account) => account.id !== currentAccount.id
      );
      const promises = await Promise.all([
        fetch(`https://hiring.tailwarden.com/v1/accounts/${currentAccount.id}`),
        fetch(
          `https://hiring.tailwarden.com/v1/accounts/${currentAccount.id}/history`
        ),
        ...otherAccounts.map((account) =>
          fetch(`https://hiring.tailwarden.com/v1/accounts/${account.id}`)
        ),
      ]);
      const [stats, history, ...otherProviderStats] = await Promise.all(
        promises.map((resp) => resp.json())
      );

      // set state when the data received
      dispatchData({ type: SET_CURRENT_STATS, stats });
      dispatchData({
        type: SET_STATS,
        stats: [
          { id: currentAccount.id, ...stats },
          ...otherAccounts.map((account, index) => ({
            id: account.id,
            ...otherProviderStats[index],
          })),
        ],
      });
      dispatchData({ type: SET_CURRENT_HISTORY, history });
    };

    currentAccount && dataFetch();
  }, [dispatchData, currentAccount, accounts]);

  document.documentElement.dir = "ltr";
  const { onOpen } = useDisclosure();
  return (
    <Box>
      <Box
        minHeight="100vh"
        height="100%"
        overflow="auto"
        position="relative"
        maxHeight="100%"
        maxWidth="100%"
        transition="all 0.33s cubic-bezier(0.685, 0.0473, 0.346, 1)"
        transitionDuration=".2s, .2s, .35s"
        transitionProperty="top, bottom, width"
        transitionTimingFunction="linear, linear, ease"
      >
        <Portal>
          <Box>
            <Navbar
              onOpen={onOpen}
              logoText={""}
              brandText="My Flaming Company"
              fixed={fixed}
              {...rest}
            />
          </Box>
        </Portal>

        <Box
          mx="auto"
          p={{ base: "20px", md: "30px" }}
          pe="20px"
          minH="100vh"
          pt="50px"
        >
          <UserReports />
        </Box>
        <Box>
          <Footer />
        </Box>
      </Box>
    </Box>
  );
}
