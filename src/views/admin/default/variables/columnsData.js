export const columnsDataCheck = [
  {
    Header: "NAME",
    accessor: "name",
  },
  {
    Header: "PROGRESS",
    accessor: "progress",
  },
  {
    Header: "QUANTITY",
    accessor: "quantity",
  },
  {
    Header: "DATE",
    accessor: "date",
  },
];
export const columnsStats = [
  {
    Header: "PROVIDER",
    accessor: "name",
  },
  {
    Header: "REGIONS",
    accessor: "regions",
  },
  {
    Header: "SERVERS",
    accessor: "servers",
  },
  {
    Header: "BILL",
    accessor: "bill",
  },
];
