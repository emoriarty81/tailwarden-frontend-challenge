import React from "react";
// Chakra Imports
import {
  Flex,
  MenuButton,
  MenuItem,
  MenuList,
  Text,
  useColorModeValue,
} from "@chakra-ui/react";
import { MdKeyboardArrowDown } from "react-icons/md";
import { ItemContent } from "components/menu/ItemContent";
import "./MenuAccountSelect.css";
import {
  useData,
  useDataDispatch,
  CHANGE_CURRENT_ACCOUNT,
} from "contexts/DataContext";

export default function MenuAccountSelect() {
  let menuBg = useColorModeValue("white", "navy.800");
  const textColor = useColorModeValue("secondaryGray.900", "white");
  const shadow = useColorModeValue(
    "14px 17px 40px 4px rgba(112, 144, 176, 0.18)",
    "14px 17px 40px 4px rgba(112, 144, 176, 0.06)"
  );
  const data = useData();
  const dispatchData = useDataDispatch();
  return (
    <>
      <MenuButton
        className="MenuAccountSelectContainer"
        p="0px"
        marginRight="50px"
        marginLeft="20px"
      >
        {data.currentAccount && (
          <span>
            <img
              style={{
                borderRadius: "5px",
                display: "inline",
                marginRight: "12px",
                maxWidth: "32px",
              }}
              src={data.currentAccount.logo}
              alt={`${data.currentAccount.provider} logo`}
            />
            <b>{data.currentAccount.provider}</b> — {data.currentAccount.label}
          </span>
        )}
        <MdKeyboardArrowDown className="inliner" />
      </MenuButton>
      <MenuList
        boxShadow={shadow}
        p="20px"
        borderRadius="20px"
        bg={menuBg}
        border="none"
        mt="22px"
        me={{ base: "30px", md: "unset" }}
        minW={{ base: "unset", md: "400px", xl: "450px" }}
        maxW={{ base: "360px", md: "unset" }}
      >
        <Flex jusitfy="space-between" w="100%" mb="20px">
          <Text fontSize="md" fontWeight="600" color={textColor}>
            Accounts
          </Text>
        </Flex>
        <Flex flexDirection="column">
          {data.accounts.map((account) => (
            <MenuItem
              key={account.id}
              _hover={{ bg: "none" }}
              _focus={{ bg: "none" }}
              px="0"
              borderRadius="8px"
              mb="10px"
              onClick={() => dispatchData({ type: CHANGE_CURRENT_ACCOUNT, account })}
            >
              <ItemContent
                info={account.label}
                title={account.provider}
                img={account.logo}
              />
            </MenuItem>
          ))}
        </Flex>
      </MenuList>
    </>
  );
}
