import React from "react";
import ReactApexChart from "react-apexcharts";

class PieChart extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      chartData: [],
      chartOptions: {},
    };
  }

  componentDidMount() {
    this.setState({
      chartData: this.props.chartData,
      chartOptions: this.props.chartOptions,
    });
  }

  componentDidUpdate() {
    if (this.state.chartData !== this.props.chartData)
      this.setState({
        chartData: this.props.chartData,
        chartOptions: this.props.chartOptions,
      });
  }

  render() {
    return (
      this.state.chartData && (
        <ReactApexChart
          options={this.state.chartOptions}
          series={this.state.chartData}
          type="pie"
          width="100%"
          height="70%"
        />
      )
    );
  }
}

export default PieChart;
