import React from "react";
import ReactDOM from "react-dom";
import "assets/css/App.css";
import AdminLayout from "layouts/admin";
import { ChakraProvider } from "@chakra-ui/react";
import theme from "theme/theme";
import { DataProvider } from "contexts/DataContext";

ReactDOM.render(
  <ChakraProvider theme={theme}>
    <React.StrictMode>
        <DataProvider>
          <AdminLayout />
        </DataProvider>
    </React.StrictMode>
  </ChakraProvider>,
  document.getElementById("root")
);
