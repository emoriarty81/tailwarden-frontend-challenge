import { createContext, useContext, useReducer } from "react";

export const SET_ACCOUNTS = "SET_ACCOUNTS";
export const CHANGE_CURRENT_ACCOUNT = "CHANGE_CURRENT_ACCOUNT";
export const SET_CURRENT_STATS = "SET_CURRENT_STATS";
export const SET_CURRENT_HISTORY = "SET_CURRENT_HISTORY";
export const SET_STATS = "SET_STATS";

export const DataContext = createContext(null);

export const DataDispatchContext = createContext(null);

export function DataProvider({ children }) {
  const [data, dispatch] = useReducer(dataReducer, initialData);

  return (
    <DataContext.Provider value={data}>
      <DataDispatchContext.Provider value={dispatch}>
        {children}
      </DataDispatchContext.Provider>
    </DataContext.Provider>
  );
}

DataProvider.displayName = "DataProvider";

export function useData() {
  return useContext(DataContext);
}

export function useDataDispatch() {
  return useContext(DataDispatchContext);
}

export function useStats() {
  const { accounts, stats } = useData();
  return stats.map((stat) => {
    const account = accounts.find((account) => account.id === stat.id);
    return {
      name: `${account.provider} (${account.label})`,
      ...stat,
    };
  });
}

function dataReducer(data, action) {
  switch (action.type) {
    case SET_ACCOUNTS: {
      return {
        ...data,
        accounts: action.accounts,
      };
    }
    case CHANGE_CURRENT_ACCOUNT: {
      return {
        ...data,
        currentAccount: action.account,
      };
    }
    case SET_CURRENT_STATS: {
      return {
        ...data,
        currentStats: action.stats,
      };
    }
    case SET_CURRENT_HISTORY: {
      return {
        ...data,
        currentHistory: action.history,
        currentHistoryGroupedByKey: [
          createChartDateFromGroup("Compute", action.history),
          createChartDateFromGroup("Storage", action.history),
          createChartDateFromGroup("Network", action.history),
        ],
        currentMonthExpenses: getCurrentMonthData(action.history),
        lastTwelveMonths: getLastTwelveMonths(action.history),
      };
    }
    case SET_STATS: {
      return {
        ...data,
        stats: action.stats,
      };
    }
    default: {
      throw Error("Unknown action: " + action.type);
    }
  }
}

const initialData = {
  accounts: [],
  currentAccount: null,
  currentHistory: [],
  currentHistoryGroupedByKey: [],
  currentMonthExpenses: [],
  lastTwelveMonths: [],
  stats: [],
};

// Tooling
function popByGroup(key, history) {
  return history
    .map((log) => log.groups.filter((group) => group.key === key))
    .map((group) => group.pop().amount);
}

function createChartDateFromGroup(key, history) {
  return {
    name: key,
    data: popByGroup(key, history),
  };
}

function getLastTwelveMonths(history) {
  const dateFormatter = new Intl.DateTimeFormat("en-US", { month: "long" });
  return history.map((log) => dateFormatter.format(new Date(log.date)));
}

function getCurrentMonthData(history) {
  const lastIndex = history.length - 1;
  return [
    history[lastIndex].groups[0].amount, // compute
    history[lastIndex].groups[1].amount, // storage
    history[lastIndex].groups[2].amount, // network
  ];
}
